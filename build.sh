#!/bin/sh

set -ex

docker build -t resume .
sudo docker run -v `pwd`:/resume -e TZ="Asia/Kolkata" resume pdflatex -output-directory /resume /resume/resume.tex
